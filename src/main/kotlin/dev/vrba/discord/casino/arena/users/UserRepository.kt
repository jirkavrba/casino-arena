package dev.vrba.discord.casino.arena.users

import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import java.math.BigInteger

@Repository
interface UserRepository : CoroutineCrudRepository<User, String> {

    fun findAllByBalanceLessThan(limit: BigInteger): Flow<User>

}