package dev.vrba.discord.casino.arena.games.slots

import dev.vrba.discord.casino.arena.games.slots.algorithm.reward.SlotMachineRewardAlgorithm
import dev.vrba.discord.casino.arena.games.slots.algorithm.spin.SlotMachineSpinAlgorithm
import org.springframework.stereotype.Service
import java.math.BigInteger

@Service
class SlotMachine(
    private val spinAlgorithm: SlotMachineSpinAlgorithm,
    private val rewardAlgorithm: SlotMachineRewardAlgorithm,
) {

    suspend fun spin(bet: BigInteger): SpinResult {
        val spin = spinAlgorithm.spin()
        val reward = rewardAlgorithm.evaluateReward(spin, bet)

        return SpinResult(spin, bet, reward)
    }

}