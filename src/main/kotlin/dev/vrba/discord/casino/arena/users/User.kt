package dev.vrba.discord.casino.arena.users

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.math.BigInteger

@Table("users")
data class User(
    @Id
    @JvmField
    @Column("user_id")
    val id: String,

    @Column("coin_balance")
    val balance: BigInteger = BigInteger.ZERO,

    @Column("is_admin")
    val admin: Boolean = false,

    @Transient
    private val new: Boolean = false,
) : Persistable<String> {
    override fun getId(): String = id
    override fun isNew(): Boolean = new
}