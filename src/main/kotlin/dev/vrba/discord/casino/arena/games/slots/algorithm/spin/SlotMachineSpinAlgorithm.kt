package dev.vrba.discord.casino.arena.games.slots.algorithm.spin

import dev.vrba.discord.casino.arena.games.slots.SlotMachineSymbol

interface SlotMachineSpinAlgorithm {

    suspend fun spin(): List<SlotMachineSymbol>

}