package dev.vrba.discord.casino.arena.error

sealed interface DomainError

object InsufficientBalanceError : DomainError