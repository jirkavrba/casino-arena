package dev.vrba.discord.casino.arena.games.slots.algorithm.spin

import dev.vrba.discord.casino.arena.games.slots.SlotMachineSymbol
import org.springframework.stereotype.Component

@Component
class SimpleSpinAlgorithm : SlotMachineSpinAlgorithm {

    override suspend fun spin(): List<SlotMachineSymbol> {
        return List(4) { SlotMachineSymbol.values().random() }
    }


}