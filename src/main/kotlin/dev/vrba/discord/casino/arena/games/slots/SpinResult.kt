package dev.vrba.discord.casino.arena.games.slots

import java.math.BigInteger

data class SpinResult(
    val symbols: List<SlotMachineSymbol>,
    val bet: BigInteger,
    val reward: BigInteger,
)