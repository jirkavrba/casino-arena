package dev.vrba.discord.casino.arena.games.slots.algorithm.reward

import dev.vrba.discord.casino.arena.games.slots.SlotMachineSymbol
import java.math.BigInteger

interface SlotMachineRewardAlgorithm {

    suspend fun evaluateReward(symbols: List<SlotMachineSymbol>, bet: BigInteger): BigInteger

}