package dev.vrba.discord.casino.arena.games.slots.gateway

import java.math.BigInteger

data class SpinRequest(
    val bet: BigInteger,
    val user: String,
)