package dev.vrba.discord.casino.arena.users

import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigInteger

@Service
class UserBalanceService(
    @Value("\${casino.balance.default}")
    private val defaultBalance: BigInteger,
    private val repository: UserRepository,
) {

    suspend fun getCoinBalance(user: String): BigInteger {
        return findOrCreateUser(user).balance
    }

    @Transactional
    suspend fun resetBalanceToDefault() {
        repository.findAll()
            .toList()
            .map {
                repository.save(it.copy(balance = defaultBalance))
            }
    }

    @Transactional
    suspend fun resetBalanceToMinimum() {
        repository.findAllByBalanceLessThan(defaultBalance)
            .toList()
            .map {
                repository.save(it.copy(balance = defaultBalance))
            }
    }

    @Transactional
    suspend fun depositCoins(user: String, amount: BigInteger): BigInteger {
        return findOrCreateUser(user).apply {
            repository.save(copy(balance = balance + amount))
        }.balance
    }

    @Transactional
    suspend fun withdrawCoins(user: String, amount: BigInteger): BigInteger {
        return findOrCreateUser(user).apply {
            repository.save(copy(balance = (balance - amount).min(BigInteger.ZERO)))
        }.balance
    }

    private suspend fun findOrCreateUser(id: String): User {
        return repository.save(
            User(
                id = id,
                balance = defaultBalance,
                admin = false,
                new = true
            )
        )
    }
}