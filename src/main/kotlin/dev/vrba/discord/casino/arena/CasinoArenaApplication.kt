package dev.vrba.discord.casino.arena

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@SpringBootApplication
class CasinoArenaApplication

fun main(args: Array<String>) {
	runApplication<CasinoArenaApplication>(*args)
}
