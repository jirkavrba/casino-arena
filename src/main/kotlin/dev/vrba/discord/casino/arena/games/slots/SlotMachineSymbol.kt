package dev.vrba.discord.casino.arena.games.slots

enum class SlotMachineSymbol {
    Seven,
    Bar,
    Cherry,
    Lemon,
    Grapes,
    Plum,
    Melon,
    Banana,
    Bell,
    Orange,
    Apple,
    FourLeafClover,
    Coin,
    Gem,
    Hearts,
    Diamonds,
    Clubs,
    Spades,
    Horseshoe,
}