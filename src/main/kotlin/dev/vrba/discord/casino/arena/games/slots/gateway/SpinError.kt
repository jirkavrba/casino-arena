package dev.vrba.discord.casino.arena.games.slots.gateway

sealed interface SpinError

object InsufficientBalanceError : SpinError