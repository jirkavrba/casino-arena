package dev.vrba.discord.casino.arena.games.slots.gateway

import dev.vrba.discord.casino.arena.games.slots.SlotMachineSymbol
import java.math.BigInteger

data class SpinResponse(
    val symbols: List<SlotMachineSymbol>,
    val bet: BigInteger,
    val reward: BigInteger,
    val balance: BigInteger
)