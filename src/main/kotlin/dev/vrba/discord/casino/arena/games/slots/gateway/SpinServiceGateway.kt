package dev.vrba.discord.casino.arena.games.slots.gateway

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import dev.vrba.discord.casino.arena.games.slots.SlotMachine
import dev.vrba.discord.casino.arena.users.UserBalanceService
import org.springframework.stereotype.Service

@Service
class SpinServiceGateway(
    private val slotMachine: SlotMachine,
    private val balanceService: UserBalanceService
) {

    suspend fun dispatch(request: SpinRequest): Either<SpinError, SpinResponse> {
        val balance = balanceService.getCoinBalance(request.user)

        if (balance <= request.bet) {
            return InsufficientBalanceError.left()
        }

        val spin = slotMachine.spin(request.bet)
        val result = balanceService.run {
            withdrawCoins(request.user, request.bet)
            depositCoins(request.user, spin.reward)
        }

        return SpinResponse(
            bet = request.bet,
            symbols = spin.symbols,
            reward = spin.reward,
            balance = result
        ).right()
    }

}