package dev.vrba.discord.casino.arena.reset

import dev.vrba.discord.casino.arena.users.UserBalanceService
import kotlinx.coroutines.runBlocking
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class WeeklyBalanceReset(
    private val balanceService: UserBalanceService
) {

    @Scheduled(cron = "0 0 0 ? * MON")
    fun run() = runBlocking {
        balanceService.resetBalanceToDefault()
        // TODO: Reset idle clickers once they are implemented
    }

}