package dev.vrba.discord.casino.arena.games.slots.algorithm.reward

import dev.vrba.discord.casino.arena.games.slots.SlotMachineSymbol
import org.springframework.stereotype.Component
import java.math.BigInteger

@Component
class SimpleRewardAlgorithm : SlotMachineRewardAlgorithm {

    override suspend fun evaluateReward(symbols: List<SlotMachineSymbol>, bet: BigInteger): BigInteger {
        val matches = symbols.groupBy { it }.values.map { it.size }
        val multiplier = matches
            .map {
                // 0, 2, 10, 100, 1000, 10000, ....
                when (it) {
                    0, 1 -> BigInteger.ZERO
                    2 -> BigInteger.TWO
                    else -> BigInteger.TEN.pow(it - 2)
                }
            }
            .filter { it >= BigInteger.ONE }
            .reduceOrNull { x, y -> x * y }
            ?: BigInteger.ZERO

        return bet * multiplier

    }

}