create table users
(
    user_id      varchar(64) not null primary key,
    user_balance bigint      not null default 0,
    is_admin     boolean     not null default false
);

create index ix_user_balance on users(user_balance);