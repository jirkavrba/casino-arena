drop index ix_user_balance;

alter table users drop column user_balance;
alter table users add column coin_balance bigint not null default 0;

create index ix_user_balance on users(coin_balance);